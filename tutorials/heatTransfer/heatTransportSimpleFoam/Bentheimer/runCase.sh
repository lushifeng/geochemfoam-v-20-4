#!/bin/bash

# ###### DO NOT MAKE CHANGES FROM HERE ###################################

set -e

NP=24

MPIRUN=mpirun 

# Decompose
echo -e "DecomposePar"
decomposePar > decomposePar.out

# Run simpleFoam in parallel
echo -e "Run simpleFoam in parallel"
$MPIRUN -np $NP simpleDBSFoam -parallel  > simpleFoam.out

for i in processor*; do mv $i/[1-9]*/U $i/0/.; done
for i in processor*; do mv $i/[1-9]*/p $i/0/.; done
for i in processor*; do mv $i/[1-9]*/phi $i/0/.; done

rm -rf processor*/[1-9]*

$MPIRUN -np $NP heatTransportSimpleFoam -parallel  > heatTransportSimpleFoam.out 

reconstructPar -latestTime

cp [1-9]*/T 0/.

rm -rf [1-9]*

rm -rf processor*

processHeatTransfer > processHeatTransfer.out




