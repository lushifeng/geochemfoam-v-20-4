#!/bin/bash

set -e

cp 0_org/alpha.air 0/alpha.air
cp 0_org/T 0/T
blockMesh
setFields
decomposePar
mpiexec -np 4 interTransportFoam -parallel
reconstructPar
rm -rf process*

processPhaseConcentration 
processInterfaceTransfer 

