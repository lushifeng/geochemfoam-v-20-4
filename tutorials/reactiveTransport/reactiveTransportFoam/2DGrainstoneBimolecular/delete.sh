#!/bin/bash

set -e

rm -f *.out
rm -rf processor*
rm -rf 0.* [1-9]* 
rm -rf 0
